
import axios from 'axios';

const CallToAPI = () => {

    return axios.get("https://my-json-server.typicode.com/Deproweb/apartment/db");

};

export default CallToAPI;
